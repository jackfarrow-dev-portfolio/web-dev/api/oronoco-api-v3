package com.oronoco.amqp;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class RabbitMqMessageProducer {
    private final AmqpTemplate amqpTemplate;
    private static final Logger logger = LoggerFactory.getLogger(RabbitMqMessageProducer.class);

    public void publish(
            Object payload,
            String exchange,
            String routingKey
    ) {
        logger.info("Publishing to {} using routing key {}. Payload: {}", exchange, routingKey,
                payload);
        amqpTemplate.convertAndSend(
                exchange,
                routingKey,
                payload
        );
        logger.info("Published to {} using routing key {}. Payload: {}", exchange, routingKey,
                payload);
    }

    public RabbitMqMessageProducer(AmqpTemplate amqpTemplate) {
        this.amqpTemplate = amqpTemplate;
    }
}
