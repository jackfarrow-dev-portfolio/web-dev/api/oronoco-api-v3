package com.oronoco.clients.notification;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

public class NotificationRequest {
    private String username;
    private String messageText;
    private String timestamp;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public NotificationRequest(String username, String messageText, String timestamp) {
        this.username = username;
        this.messageText = messageText;
        this.timestamp = timestamp;
    }

    public NotificationRequest() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NotificationRequest that = (NotificationRequest) o;
        return Objects.equals(username, that.username) && Objects.equals(messageText, that.messageText) && Objects.equals(timestamp, that.timestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, messageText, timestamp);
    }

    @Override
    public String toString() {
        return "NotificationRequest{" +
                "username='" + username + '\'' +
                ", messageText='" + messageText + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}
