package com.oronoco.clients.auth;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.UUID;

@FeignClient("auth")
public interface AuthClient {

    @GetMapping(path = "api/v3/auth/{customerId}")
    boolean isAuthenticated(@PathVariable("customerId")UUID customerId);
}
