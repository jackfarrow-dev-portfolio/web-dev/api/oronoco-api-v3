package com.oronoco.notification;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class NotificationService {

    private final NotificationRepository notificationRepository;

    @Autowired
    public NotificationService(NotificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }

    public void createNotification(Notification notification) {
        this.notificationRepository.save(notification);
    }

    public Optional<Notification> getNotificationById(UUID notificationId) {
        return this.notificationRepository.findById(notificationId);
    }
}
