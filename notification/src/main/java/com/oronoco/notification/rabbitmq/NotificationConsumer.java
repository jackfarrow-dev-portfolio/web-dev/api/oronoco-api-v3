package com.oronoco.notification.rabbitmq;

import com.oronoco.clients.notification.NotificationRequest;
import com.oronoco.notification.Notification;
import com.oronoco.notification.NotificationService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@Slf4j
public class NotificationConsumer {

    private final NotificationService notificationService;
    private final Logger logger = LoggerFactory.getLogger(NotificationConsumer.class);
    @Autowired
    public NotificationConsumer(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @RabbitListener(
            queues = "${rabbitmq.queue.notification}"
    )
    public void consumer(NotificationRequest notificationRequest) {
        logger.info("Consumed {} from queue", notificationRequest);
        notificationService.createNotification(
                new Notification(
                        notificationRequest.getUsername(),
                        notificationRequest.getMessageText(),
                        LocalDateTime.now()
                )
        );
    }
}
