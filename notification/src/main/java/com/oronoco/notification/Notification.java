package com.oronoco.notification;

import org.hibernate.annotations.GenericGenerator;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.*;

@Entity
@Table(name = "notification")
public class Notification {
    @Id
    @Column(name = "notification_id")
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID notificationId;

    @Column(name = "username")
    private String username;

    @Column(name = "message_text")
    private String messageText;

    @Column(name = "timestamp")
    private LocalDateTime timestamp;

    public Notification() {
    }

    public Notification(
            String username,
            String messageText,
            LocalDateTime timestamp
    ) {
        this.username = username;
        this.messageText = messageText;
        this.timestamp = timestamp;
    }

    public Notification(
            UUID notificationId,
            String username,
            String messageText,
            LocalDateTime timestamp
    ) {
        this.notificationId = notificationId;
        this.username = username;
        this.messageText = messageText;
        this.timestamp = timestamp;
    }

    public UUID getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(UUID notificationId) {
        this.notificationId = notificationId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Notification that = (Notification) o;
        return Objects.equals(notificationId, that.notificationId) && Objects.equals(username, that.username) && Objects.equals(messageText, that.messageText) && Objects.equals(timestamp, that.timestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(notificationId, username, messageText, timestamp);
    }

    @Override
    public String toString() {
        return "Notification{" +
                "notificationId=" + notificationId +
                ", username='" + username + '\'' +
                ", messageText='" + messageText + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}
