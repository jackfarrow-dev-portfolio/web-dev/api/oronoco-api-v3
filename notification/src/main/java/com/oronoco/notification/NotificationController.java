package com.oronoco.notification;

import com.oronoco.clients.notification.NotificationRequest;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequestMapping("api/v3/notification")
@Slf4j
public class NotificationController {

    private final NotificationService notificationService;
    private final Logger logger = LoggerFactory.getLogger(NotificationController.class);
    public NotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @PostMapping
    public void createNotification(@RequestBody NotificationRequest request) {
        logger.info("Received request to create notification: " + request.getMessageText()
            + " at " + request.getTimestamp()
        );
        this.notificationService.createNotification(
                new Notification(
                        request.getUsername(),
                        request.getMessageText(),
                        LocalDateTime.now()
                )
        );
    }
}
