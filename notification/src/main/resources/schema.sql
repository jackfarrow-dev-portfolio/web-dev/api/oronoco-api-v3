CREATE TABLE IF NOT EXISTS notification (
    notification_id UUID DEFAULT random_uuid() PRIMARY KEY,
    customer_id UUID NOT NULL,
    message_text VARCHAR (250) NOT NULL,
    timestamp TIMESTAMP NOT NULL
);