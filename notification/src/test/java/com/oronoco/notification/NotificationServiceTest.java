package com.oronoco.notification;

import org.aspectj.weaver.ast.Not;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.then;


@DataJpaTest
class NotificationServiceTest {

    @Mock
    private NotificationRepository notificationRepository;

    @Captor
    private ArgumentCaptor<Notification> notificationArgumentCaptor;
    public NotificationService underTest;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        underTest = new NotificationService(notificationRepository);
    }

    @Test
    public void itShouldCreateANewNotification() {
        // Given
        String username = "test-user123";
        String messageText = "This is a message";
        LocalDateTime timestamp = LocalDateTime.now();

        Notification notification = new Notification(
                username,
                messageText,
                timestamp
        );
        // When
        underTest.createNotification(notification);
        // Then
        then(notificationRepository).should().save(notificationArgumentCaptor.capture());
        Notification savedNotification = notificationArgumentCaptor.getValue();
        assertThat(savedNotification).isEqualTo(notification);
    }
}