CREATE TABLE IF NOT EXISTS product (
    product_id UUID DEFAULT random_uuid() PRIMARY KEY,
    product_name VARCHAR (50) NOT NULL,
    description VARCHAR (500) NOT NULL,
    unit_cost FLOAT NOT NULL,
    retail_price FLOAT NOT NULL,
    manufacturer VARCHAR (125) NOT NULL,
    product_image_url VARCHAR(250),
    product_thumbnail_image_url VARCHAR(250)
);