package com.oronoco.product;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface ProductRepository extends CrudRepository<Product, UUID> {

    @Query("SELECT p FROM Product p WHERE p.productId = ?1")
    Optional<Product> getProductByProductId(@Param("productId")UUID productId);

    @Transactional
    @Modifying
    @Query("DELETE FROM Product p WHERE p.productId = ?1")
    void deleteProductByProductId(@Param("productId") UUID productId);
}
