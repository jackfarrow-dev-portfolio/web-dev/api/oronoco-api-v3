package com.oronoco.product;

import com.oronoco.exception.ApiRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("api/v3/product")
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("all")
    public Iterable<Product> getAllProducts() {
        return this.productService.getAllProducts();
    }

    @GetMapping
    public Optional<Product> getProductByProductId(@Param("productId") String productId) {
        try {
            return this.productService.getProductByProductId(UUID.fromString(productId));
        } catch (IllegalArgumentException e) {
            throw new ApiRequestException("productId " + productId + " is invalid or malformed");
        }

    }

    @PostMapping("create")
    public void createProduct(@RequestBody ProductRequest request) {
        this.productService.createProduct(request);
    }

    @PutMapping("update")
    public void updateProductDetails(@RequestBody ProductRequest request) {
        this.productService.updateProductDetails(request);
    }

    @DeleteMapping("delete")
    public void deleteProductByProductId(@RequestBody ProductRequest request) {
        this.productService.deleteProductByProductId(request);
    }
}
