package com.oronoco.product;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;


import java.util.Objects;
import java.util.UUID;

@Entity(name = "Product")
@Table(name = "product")
public class Product {

    @Id
    @Column(name = "product_id")
    private UUID productId;
    @Column(name = "product_name")
    private String productName;
    @Column(name = "description")
    private String description;
    @Column(name = "unit_cost")
    private float unitCost;
    @Column(name = "retail_price")
    private float retailPrice;
    @Column(name = "product_image_url")
    private String productImageUrl;
    @Column(name = "product_thumbnail_image_url")
    private String productThumbnailImageUrl;
    @Column(name = "manufacturer")
    private String manufacturer;

    public Product() {
    }

    public Product(
            String productName,
            String description,
            float unitCost,
            float retailPrice,
            String productImageUrl,
            String productThumbnailImageUrl,
            String manufacturer
    ) {
        this.productName = productName;
        this.description = description;
        this.unitCost = unitCost;
        this.retailPrice = retailPrice;
        this.productImageUrl = productImageUrl;
        this.productThumbnailImageUrl = productThumbnailImageUrl;
        this.manufacturer = manufacturer;
    }

    public Product(
            UUID productId,
            String productName,
            String description,
            float unitCost,
            float retailPrice,
            String productImageUrl,
            String productThumbnailImageUrl,
            String manufacturer
    ) {
        this.productId = productId;
        this.productName = productName;
        this.description = description;
        this.unitCost = unitCost;
        this.retailPrice = retailPrice;
        this.productImageUrl = productImageUrl;
        this.productThumbnailImageUrl = productThumbnailImageUrl;
        this.manufacturer = manufacturer;
    }

    public UUID getProductId() {
        return productId;
    }

    public void setProductId(UUID productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getUnitCost() {
        return unitCost;
    }

    public void setUnitCost(float unitCost) {
        this.unitCost = unitCost;
    }

    public float getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(float retailPrice) {
        this.retailPrice = retailPrice;
    }

    public String getProductImageUrl() {
        return productImageUrl;
    }

    public void setProductImageUrl(String productImageUrl) {
        this.productImageUrl = productImageUrl;
    }

    public String getProductThumbnailImageUrl() {
        return productThumbnailImageUrl;
    }

    public void setProductThumbnailImageUrl(String productThumbnailImageUrl) {
        this.productThumbnailImageUrl = productThumbnailImageUrl;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Float.compare(unitCost, product.unitCost) == 0 && Float.compare(retailPrice, product.retailPrice) == 0 && Objects.equals(productId, product.productId) && Objects.equals(productName, product.productName) && Objects.equals(description, product.description) && Objects.equals(productImageUrl, product.productImageUrl) && Objects.equals(productThumbnailImageUrl, product.productThumbnailImageUrl) && Objects.equals(manufacturer, product.manufacturer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productId, productName, description, unitCost, retailPrice, productImageUrl, productThumbnailImageUrl, manufacturer);
    }

    @Override
    public String toString() {
        return "Product{" +
                "productId=" + productId +
                ", productName='" + productName + '\'' +
                ", description='" + description + '\'' +
                ", unitCost=" + unitCost +
                ", retailPrice=" + retailPrice +
                ", productImageUrl='" + productImageUrl + '\'' +
                ", productThumbnailImageUrl='" + productThumbnailImageUrl + '\'' +
                ", manufacturer='" + manufacturer + '\'' +
                '}';
    }
}
