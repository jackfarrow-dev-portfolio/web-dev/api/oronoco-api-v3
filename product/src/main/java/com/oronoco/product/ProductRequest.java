package com.oronoco.product;

import java.util.UUID;

public class ProductRequest {
    private UUID requestorId;
    private Product product;

    public ProductRequest() {
    }

    public ProductRequest(UUID requestorId, Product product) {
        this.requestorId = requestorId;
        this.product = product;
    }

    public Product getProduct() {
        return product;
    }

    public UUID getRequestorId() {
        return requestorId;
    }
}
