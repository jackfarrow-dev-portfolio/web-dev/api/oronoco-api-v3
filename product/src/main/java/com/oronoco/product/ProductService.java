package com.oronoco.product;

import com.oronoco.clients.auth.AuthClient;
import com.oronoco.exception.NotAuthenticatedException;
import com.oronoco.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.Optional;
import java.util.UUID;

@Service
public class ProductService {

    private final ProductRepository productRepository;
    private final AuthClient authClient;
    @Autowired
    public ProductService(
            ProductRepository productRepository,
            AuthClient authClient
            )
    {
        this.productRepository = productRepository;
        this.authClient = authClient;
    }

    public Iterable<Product> getAllProducts() {
        return this.productRepository.findAll();
    }

    public Optional<Product> getProductByProductId(UUID productId) {
            Optional<Product> optionalProduct = this.productRepository.getProductByProductId(productId);
            if (optionalProduct.isEmpty()) {
                throw new NotFoundException("Unable to find product with id " + productId);
            }
            return optionalProduct;
    }

    public void createProduct(ProductRequest request) {
        if(!this.authClient.isAuthenticated(request.getRequestorId())) {
            throw new NotAuthenticatedException("Customer identified by " + request.getRequestorId() +
                    " is not authorized to perform this operation");
        }
        this.productRepository.save(request.getProduct());
    }

    public void updateProductDetails(ProductRequest request) {
        if(!this.authClient.isAuthenticated(request.getRequestorId())) {
            throw new NotAuthenticatedException("Customer identified by " + request.getRequestorId() +
                    " is not authorized to perform this operation");
        }
        this.productRepository.save(request.getProduct());
    }

    public void deleteProductByProductId(ProductRequest request) {
        if (!authClient.isAuthenticated(request.getRequestorId())) {
            throw new NotAuthenticatedException("Customer identified by " + request.getRequestorId() +
                    " is not authorized to perform this operation");
        }
        Optional<Product> optionalProduct =
                this.getProductByProductId(request.getProduct().getProductId());
        if(optionalProduct.isEmpty()) {
            throw new NotFoundException("Product " + request.getProduct().getProductId() + " was " +
                    "not found");
        }
        this.productRepository.deleteProductByProductId(request.getProduct().getProductId());
    }
}
