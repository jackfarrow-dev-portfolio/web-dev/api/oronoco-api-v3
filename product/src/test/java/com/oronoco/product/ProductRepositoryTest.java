package com.oronoco.product;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@DataJpaTest
class ProductRepositoryTest {
    @Autowired
    private ProductRepository underTest;

    @Test
    void itShouldGetAProductByProductId() {
        // Given
        Long id = 1l;
        UUID productId = UUID.randomUUID();
        String productName = "Large Widget";
        String productDescription = "This is the largest widget we've ever made!";
        float unitCost = 0.01f;
        float retailPrice = 99.95f;
        String imageUrl = "https://fakesite.imagehost.com/1234";
        String thumbnailUrl = "https://fakesite.imagehost.com/1234/thumbnail";
        String manufacturer = "WidgetCorp";

        Product product = new Product(
                productId,
                productName,
                productDescription,
                unitCost,
                retailPrice,
                imageUrl,
                thumbnailUrl,
                manufacturer
        );

        // When
        underTest.save(product);
        Optional<Product> optionalProduct = underTest.getProductByProductId(productId);

        // Then
        assertThat(optionalProduct)
                .isPresent()
                .hasValueSatisfying(p -> {
                    assertThat(p).isEqualToComparingFieldByField(product);
                });

    }

    @Test
    public void itShouldDeleteAProductByProductId() {
        // Given
        UUID productId = UUID.randomUUID();
        String productName = "Large Widget";
        String productDescription = "This is the largest widget we've ever made!";
        float unitCost = 0.01f;
        float retailPrice = 99.95f;
        String imageUrl = "https://fakesite.imagehost.com/1234";
        String thumbnailUrl = "https://fakesite.imagehost.com/1234/thumbnail";
        String manufacturer = "WidgetCorp";

        Product product = new Product(
                productId,
                productName,
                productDescription,
                unitCost,
                retailPrice,
                imageUrl,
                thumbnailUrl,
                manufacturer
        );
        // When
        underTest.save(product);
        Optional<Product> optionalProduct = underTest.getProductByProductId(productId);
        assertThat(optionalProduct.get())
                .isEqualToComparingFieldByField(product);
        underTest.deleteProductByProductId(productId);
        // Then
        Optional<Product> deletedProduct = underTest.getProductByProductId(productId);
        assertThat(deletedProduct)
                .isEmpty();

    }
}