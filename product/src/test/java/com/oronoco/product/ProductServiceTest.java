package com.oronoco.product;

import com.oronoco.clients.auth.AuthClient;
import com.oronoco.exception.NotAuthenticatedException;
import com.oronoco.exception.NotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.assertj.core.api.Assertions.*;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@DataJpaTest
public class ProductServiceTest {

    @Mock
    private ProductRepository productRepository;

    @Mock
    private AuthClient authClient;

    @Captor
    private ArgumentCaptor<Product> productArgumentCaptor;

    private ProductService underTest;

    @BeforeEach
    void setup() {
        MockitoAnnotations.initMocks(this);
        underTest = new ProductService(productRepository, authClient);
    }

    @Test
    void itShouldGetAllProducts() {
        // Given
        Long id = 1l;
        UUID productId = UUID.randomUUID();
        String productName = "Large Widget";
        String productDescription = "This is the largest widget we've ever made!";
        float unitCost = 0.01f;
        float retailPrice = 99.95f;
        String imageUrl = "https://fakesite.imagehost.com/1234";
        String thumbnailUrl = "https://fakesite.imagehost.com/1234/thumbnail";
        String manufacturer = "WidgetCorp";

        Product product = new Product(
                productId,
                productName,
                productDescription,
                unitCost,
                retailPrice,
                imageUrl,
                thumbnailUrl,
                manufacturer
        );

        List<Product> results = new ArrayList<>();
        results.add(product);
        given(productRepository.findAll()).willReturn(results);

        // When
        Iterable<Product> productIterable = underTest.getAllProducts();

        // Then
        productIterable.forEach(i -> {
            assertThat(i)
                    .isEqualTo(product);
        });
    }

    @Test
    void itShouldGetAProductByProductId() {
        // Given
        UUID productId = UUID.randomUUID();
        String productName = "Smallest Widget";
        String productDescription = "This is the smallest widget we've ever made!";
        float unitCost = 0.01f;
        float retailPrice = 199.95f;
        String imageUrl = "https://fakesite.imagehost.com/1234";
        String thumbnailUrl = "https://fakesite.imagehost.com/1234/thumbnail";
        String manufacturer = "WidgetCorp";

        Product product = new Product(
                productId,
                productName,
                productDescription,
                unitCost,
                retailPrice,
                imageUrl,
                thumbnailUrl,
                manufacturer
        );

       given(productRepository.getProductByProductId(productId)).willReturn(Optional.of(product));

       // When
        Optional<Product> optionalProduct = underTest.getProductByProductId(productId);

        // Then
        assertThat(optionalProduct)
                .isPresent()
                .hasValueSatisfying(p -> {
                    assertThat(p)
                            .isEqualTo(product);
                });
    }

    @Test
    public void itShouldThrowNotFoundExceptionWhenProductIdNotFound() {
        // Given
        UUID badId = UUID.randomUUID();
        String productName = "Smallest Widget";
        String productDescription = "This is the smallest widget we've ever made!";
        float unitCost = 0.01f;
        float retailPrice = 199.95f;
        String imageUrl = "https://fakesite.imagehost.com/1234";
        String thumbnailUrl = "https://fakesite.imagehost.com/1234/thumbnail";
        String manufacturer = "WidgetCorp";

        Product product = new Product(
                badId,
                productName,
                productDescription,
                unitCost,
                retailPrice,
                imageUrl,
                thumbnailUrl,
                manufacturer
        );

        given(productRepository.getProductByProductId(badId)).willReturn(Optional.empty());

        // When
        // Then
        assertThatThrownBy(() -> underTest.getProductByProductId(badId))
                .isInstanceOf(NotFoundException.class);
    }

    @Test
    public void itShouldCreateAProductWhenCustomerIsAuthorized() {
        // Given
        UUID productId = UUID.randomUUID();
        UUID customerId = UUID.randomUUID();
        String productName = "Smallest Widget";
        String productDescription = "This is the smallest widget we've ever made!";
        float unitCost = 0.01f;
        float retailPrice = 199.95f;
        String imageUrl = "https://fakesite.imagehost.com/1234";
        String thumbnailUrl = "https://fakesite.imagehost.com/1234/thumbnail";
        String manufacturer = "WidgetCorp";

        Product product = new Product(
                productId,
                productName,
                productDescription,
                unitCost,
                retailPrice,
                imageUrl,
                thumbnailUrl,
                manufacturer
        );

        // When
        when(authClient.isAuthenticated(customerId)).thenReturn(true);
        underTest.createProduct(new ProductRequest(customerId, product));

        // Then
        then(productRepository).should().save(productArgumentCaptor.capture());
        Product savedProduct = productArgumentCaptor.getValue();
        assertThat(savedProduct).isEqualTo(product);
    }

    @Test
    public void itShouldNotCreateAProductWhenCustomerIsNotAuthorized() {
        // Given
        UUID productId = UUID.randomUUID();
        UUID customerId = UUID.randomUUID();
        String productName = "Smallest Widget";
        String productDescription = "This is the smallest widget we've ever made!";
        float unitCost = 0.01f;
        float retailPrice = 199.95f;
        String imageUrl = "https://fakesite.imagehost.com/1234";
        String thumbnailUrl = "https://fakesite.imagehost.com/1234/thumbnail";
        String manufacturer = "WidgetCorp";

        Product product = new Product(
                productId,
                productName,
                productDescription,
                unitCost,
                retailPrice,
                imageUrl,
                thumbnailUrl,
                manufacturer
        );

        // When
        when(authClient.isAuthenticated(customerId)).thenReturn(false);

        // Then
        assertThatExceptionOfType(NotAuthenticatedException.class).isThrownBy(
                () -> underTest.createProduct(new ProductRequest(customerId, product))
        ).withMessage("Customer identified by " + customerId +
                " is not authorized to perform this operation");
        // Finally
        then(productRepository).shouldHaveNoInteractions();
    }

    @Test
    public void itShouldUpdateAProductWhenCustomerIsAuthorized() {
        // Given
        UUID productId = UUID.randomUUID();
        UUID customerId = UUID.randomUUID();
        String productName = "Smallest Widget";
        String productDescription = "This is the smallest widget we've ever made!";
        float unitCost = 0.01f;
        float retailPrice = 199.95f;
        String imageUrl = "https://fakesite.imagehost.com/1234";
        String thumbnailUrl = "https://fakesite.imagehost.com/1234/thumbnail";
        String manufacturer = "WidgetCorp";

        Product product = new Product(
                productId,
                productName,
                productDescription,
                unitCost,
                retailPrice,
                imageUrl,
                thumbnailUrl,
                manufacturer
        );

        // When
        when(authClient.isAuthenticated(customerId)).thenReturn(true);
        underTest.updateProductDetails(new ProductRequest(customerId, product));

        // Then
        then(productRepository).should().save(productArgumentCaptor.capture());
        Product savedProduct = productArgumentCaptor.getValue();
        assertThat(savedProduct).isEqualTo(product);
    }

    @Test
    public void itShouldNotUpdateAProductWhenCustomerNotAuthorized() {
        // Given
        UUID productId = UUID.randomUUID();
        UUID customerId = UUID.randomUUID();
        String productName = "Smallest Widget";
        String productDescription = "This is the smallest widget we've ever made!";
        float unitCost = 0.01f;
        float retailPrice = 199.95f;
        String imageUrl = "https://fakesite.imagehost.com/1234";
        String thumbnailUrl = "https://fakesite.imagehost.com/1234/thumbnail";
        String manufacturer = "WidgetCorp";

        Product product = new Product(
                productId,
                productName,
                productDescription,
                unitCost,
                retailPrice,
                imageUrl,
                thumbnailUrl,
                manufacturer
        );

        // When
        when(authClient.isAuthenticated(customerId)).thenReturn(false);

        // Then
        assertThatExceptionOfType(NotAuthenticatedException.class).isThrownBy(
                () -> underTest.updateProductDetails(new ProductRequest(customerId, product))
        ).withMessage("Customer identified by " + customerId +
                " is not authorized to perform this operation");
        // Finally
        then(productRepository).shouldHaveNoInteractions();
    }

    @Test
    public void itShouldDeleteAProductWhenCustomerIsAuthorized() {
        // Given
        UUID productId = UUID.randomUUID();
        UUID customerId = UUID.randomUUID();
        String productName = "Smallest Widget";
        String productDescription = "This is the smallest widget we've ever made!";
        float unitCost = 0.01f;
        float retailPrice = 199.95f;
        String imageUrl = "https://fakesite.imagehost.com/1234";
        String thumbnailUrl = "https://fakesite.imagehost.com/1234/thumbnail";
        String manufacturer = "WidgetCorp";

        Product product = new Product(
                productId,
                productName,
                productDescription,
                unitCost,
                retailPrice,
                imageUrl,
                thumbnailUrl,
                manufacturer
        );

        // When
        ProductRequest productRequest = new ProductRequest(customerId, product);
        when(authClient.isAuthenticated(customerId)).thenReturn(true);
        underTest.createProduct(productRequest);

        // Then
    }
}
