# Data Model

### Product
| Field                 | Type   |
|-----------------------|--------|
| productId _PK_        | UUID   |
| productName           | String |
| description           | String |
| unitCost              | Float  |
| retailPrice           | Float  |
| manufacturer          | String |
| productImage          | String |
| productThumbnailImage | String |

### Inventory

| Field            | Type |
|------------------|------|
| inventoryId _PK_ | UUID |
| productId _FK_   | UUID |
| count            | Int  |

### Customer

| Field          | Type    |
|----------------|---------|
| customerId _PK_ | UUID    |
| firstName      | String  |
| lastName       | String  |
| billingAddress | String  |
| municipality   | String  |
| stateProvince  | String  |
| postalCode     | String  |
| gender         | String  |
| emailAddress   | String  |
| phoneNumber    | String  |
|date_of_birth | Date |
| userName | String |
| password | String |

### Review

| Field           | Type    |
|-----------------|---------|
| reviewId _PK_   | UUID    |
| productId _FK_  | UUID    |
| customerId _FK_ | UUID    |
| reviewText      | String  |
| starsOutOfFive  | Integer |
| reviewDate      | Date    |

### Purchase

| Field                        | Type   |
|------------------------------|--------|
| purchaseId _PK_              | UUID   |
| customerId _FK_              | UUID   |
| purchaseDate                 | Date   |
| totalCost                    | Float  |
| shippingMethod               | String |
| shippingAddress1             | String |
| shippingAddress2             | String |
| shippingAddressMunicipality  | String |
| shippingAddressStateProvince | String |
| shippingAddressPostalCode    | String |
| shippingAddressCountry       | String |
| billingAddress1              | String |
| billingAddress2              | String |
| billingAddressMunicipality   | String |
| billingAddressStateProvince  | String |
| billingAddressPostalCode     | String |
| billingAddressCountry        | String |

#### Purchase-Product

| Field           | Type |
|-----------------|------|
| id              | UUID |
| purchaseId _FK_ | UUID |
| productId _FK_  | UUID |


### Payment

| Field           | Type    |
|-----------------|---------|
| paymentId _PK_  | UUID    |
| purchaseId _FK_ | UUID    |
| amount          | Float   |
| purchaseDate    | Date    |
| paymentMethod   | String  |
| isAuthorized    | Boolean |

### Account

| Field           | Type    |
|-----------------|---------|
| accountId _PK_  | UUID    |
| customerId _FK_ | UUID    |
| createDate      | Date    |
| isActive        | Boolean |
| isSeller        | Boolean |

#### Purchase-Account

| Field           | Type |
|-----------------|------|
| id              | UUID |
| purchaseId _FK_ | UUID |
| accountId _FK_  | UUID |

- **_PK_**: Primary Key
- **_FK_**: Foreign Key