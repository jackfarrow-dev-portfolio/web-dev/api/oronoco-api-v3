# API Operations

## Authentication/Authorization
| Auth       |
|------------|
| Login _M_  |
| Logout _M_ |


## Products

| Product                       |
|-------------------------------|
| Get Products _M_              |
| Get Product By (name, id) _M_ |
| Create Product _AT_, _AZ_     |
| Update Product  _AT_, _AZ_    |
| Delete Product  _AT_, _AZ_    |

## Customers

| Customer                                        |
|-------------------------------------------------|
| Get Customer By Username and Email _M_          |
| Create New Customer _M_                         |
| Update Customer Details _AT_, _AZ_              |
| Delete Customer By Username and Email_AT_, _AZ_ |

## Notifications
| Notification |
|-|
|Create Customer Registration Notification _M_ |


## Reviews

| Review                        |
|-------------------------------|
| Create Review of Product _AT_ |
| Get Reviews of Product _M_    |
| Create Upvote _AT_            |
| Get Reviews by Customer _M_   |

## Purchases

| Purchase                         |
|----------------------------------|
| Create Purchase _M_              |
| View Purchase History _AT_, _AZ_ |

## Accounts

| Account                           |
|-----------------------------------|
| Create Account _M_                |
| Update Account Details _AT_, _AZ_ |
| Suspend Account _AT_, _AZ_        |
| Delete Account  _AT_, _AZ_        |


- **_M_**: MVP
- **_AT_**: Authenticated Users
- **_AZ_**: Authorized Users