terraform {
  required_providers {
    aws = {
      source: "hashicorp/aws"
      version: "5.20.0"
    }
  }
}
provider "aws" {
  region = "us-east-2"
  profile = "terraform"
}

variable "cidr_blocks" {
  description = "cidr blocks for subnets"
  type = list(
    object(
      {
        cidr_block = string,
        name = string,
        availability_zone = string
      }
    )
  )
}


resource "aws_vpc" "oronoco_api_v3_vpc" {
  cidr_block = "192.168.0.0/16"
  tags = {
    Name = "oronoco_api_v3_vpc"
    create_date = "2023-10-08"
    environment = "dev"
  }
}

resource "aws_subnet" "oronoco_public_sn_2a" {
  vpc_id = aws_vpc.oronoco_api_v3_vpc.id
  cidr_block = var.cidr_blocks[0].cidr_block
  availability_zone = var.cidr_blocks[0].availability_zone
  tags = {
    Name = var.cidr_blocks[0].name
    belongs_to = aws_vpc.oronoco_api_v3_vpc.tags.Name
    create_date = "2023-10-08"
    environment = "dev"
  }
}

resource "aws_subnet" "oronoco_public_sn_2b" {
  vpc_id = aws_vpc.oronoco_api_v3_vpc.id
  cidr_block = var.cidr_blocks[1].cidr_block
  availability_zone = var.cidr_blocks[1].availability_zone
  tags = {
    Name = var.cidr_blocks[1].name
    belongs_to = aws_vpc.oronoco_api_v3_vpc.tags.Name
    create_date = "2023-10-08"
    environment = "dev"
  }
}

resource "aws_internet_gateway" "oronoco_api_v3_igwy" {
  vpc_id = aws_vpc.oronoco_api_v3_vpc.id
  tags = {
    Name = "oronoco_api_v3_igwy"
    belongs_to = aws_vpc.oronoco_api_v3_vpc.tags.Name
    create_date = "2023-10-08"
    environment = "dev"
  }
}

resource "aws_route_table_association" "main_rtb_association_sn_2a" {
  route_table_id = aws_vpc.oronoco_api_v3_vpc.main_route_table_id
  subnet_id = aws_subnet.oronoco_public_sn_2a.id
}

resource "aws_route_table_association" "main_rtb_association_2b" {
  route_table_id = aws_vpc.oronoco_api_v3_vpc.main_route_table_id
  subnet_id = aws_subnet.oronoco_public_sn_2b.id
}

resource "aws_route" "main_rtb_ipv4_egress" {
  route_table_id = aws_vpc.oronoco_api_v3_vpc.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.oronoco_api_v3_igwy.id
}

resource "aws_security_group" "public_ssh_for_ddb_mgmt" {
  vpc_id = aws_vpc.oronoco_api_v3_vpc.id
  name = "public_ssh_for_ddb_mgmt"
  description = "Allow SSH from public to EC2 instances connecting to DynamoDB"

  ingress {
    description = "SSH from public"
    from_port = 22
    to_port = 22
    protocol = 6
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "public_ssh_for_ddb_mgmt"
    belongs_to = aws_vpc.oronoco_api_v3_vpc.tags.Name
    create_date = "2023-10-08"
    environment = "dev"
  }
}