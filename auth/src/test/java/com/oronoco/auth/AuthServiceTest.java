package com.oronoco.auth;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class AuthServiceTest {

    private AuthService underTest;

    @BeforeEach
    public void setup() {
        this.underTest = new AuthService();
    }
    @Test
    public void itShouldReturnTrueIfCustomerIsAuthenticated() {
        // Given
        UUID customerId = UUID.randomUUID();
        // When
        boolean isAuthenticated = underTest.isCustomerAuthenticated(customerId);
        // Then
        assertTrue(isAuthenticated);
    }
}