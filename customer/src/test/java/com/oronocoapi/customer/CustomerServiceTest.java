package com.oronocoapi.customer;

import com.oronoco.amqp.RabbitMqMessageProducer;
import com.oronoco.clients.auth.AuthClient;
import com.oronoco.clients.notification.NotificationClient;
import com.oronoco.exception.BadRequestException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

class CustomerServiceTest {

    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private AuthClient authClient;
    @Mock
    private RabbitMqMessageProducer rabbitMqMessageProducer;

    private CustomerService underTest;

    @BeforeEach
    void setup() {
        MockitoAnnotations.initMocks(this);
        underTest = new CustomerService(
                customerRepository,
                authClient,
                rabbitMqMessageProducer
        )
        ;
    }

    @Captor
    private ArgumentCaptor<Customer> customerArgumentCaptor;

    @Test
    void itShouldGetExpectedCustomerByUsernameAndEmail() {
        // Given
        UUID uuid = UUID.randomUUID();
        String username = "testUser123";
        String email = "test_user@fakemail.com";
        String firstName = "Test";
        String lastName = "User";
        String streetAddress = "1234 Elm Street";
        String postalCode = "00000";
        String city = "Springfield";
        String stateProvince = "OH";
        String phoneNumber = "+1 000 000 0000";
        String gender = "Female";
        String password = "secret_password";

        Customer customer = new Customer(
                uuid,
                firstName,
                lastName,
                streetAddress,
                city,
                stateProvince,
                postalCode,
                email,
                phoneNumber,
                LocalDate.of(1999, 12, 31),
                gender,
                username,
                password
        );

        given(customerRepository.getCustomerByUsernameAndEmail(username, email)).willReturn(Optional.of(customer));
        // When
        Optional<Customer> optionalCustomer = underTest.getCustomerByUsernameAndEmail(username, email);

        // Then
        assertThat(optionalCustomer)
                .isPresent()
                .hasValueSatisfying(c -> {
                    assertThat(c)
                            .isEqualTo(customer);
                });
    }

    @Test
    public void itShouldNotSaveAnAlreadyRegisteredCustomer() {
        // Given
        UUID uuid = UUID.randomUUID();
        String username = "testUser123";
        String email = "test_user@fakemail.com";
        String firstName = "Test";
        String lastName = "User";
        String streetAddress = "1234 Elm Street";
        String postalCode = "00000";
        String city = "Springfield";
        String stateProvince = "OH";
        String phoneNumber = "+1 000 000 0000";
        String gender = "Female";
        String password = "secret_password";

        Customer customer = new Customer(
                uuid,
                firstName,
                lastName,
                streetAddress,
                city,
                stateProvince,
                postalCode,
                email,
                phoneNumber,
                LocalDate.of(1999, 12, 31),
                gender,
                username,
                password
        );

        given(customerRepository.getCustomerByUsernameAndEmail(username, email)).willReturn(Optional.of(customer));

        // When
        // Then
        assertThatExceptionOfType(BadRequestException.class)
                .isThrownBy(
                        () -> underTest.registerCustomer(customer)
                ).withMessage("Unable to register customer; username %s is already " +
                       "taken", username);

        // Finally
        then(customerRepository).should().getCustomerByUsernameAndEmail(username, email);
        then(customerRepository).shouldHaveNoMoreInteractions();
    }
}