package com.oronocoapi.customer;

import org.checkerframework.checker.units.qual.C;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class CustomerRepositoryTest {

    @Autowired
    CustomerRepository underTest;

    @Test
    void itShouldSelectCustomerByUsernameAndEmailAddress() {
        // Given
        UUID uuid = UUID.randomUUID();
        String firstName = "Test";
        String lastName = "User";
        String address = "1234 Main Street";
        String city = "Springfield";
        String state = "IL";
        String postalCode = "12345";
        String emailAddress = "test_user@gmail.com";
        String phoneNumber = "+0-000-0000";
        LocalDate dateOfBirth = LocalDate.of(1991, 3, 3);
        String gender = "Male";
        String username = "testUser123";
        String password = "secret";

        Customer customer = new Customer(
                firstName,
                lastName,
                address,
                city,
                state,
                postalCode,
                emailAddress,
                phoneNumber,
                dateOfBirth,
                gender,
                username,
                password
        );
        // When
        underTest.save(customer);
        // Then
        Optional<Customer> optionalCustomer = underTest.getCustomerByUsernameAndEmail(username,
                emailAddress);
        assertThat(optionalCustomer)
                .isPresent()
                .hasValueSatisfying(c -> {
                    assertThat(c)
                            .isEqualToComparingFieldByField(customer);
                });
    }

    @Test
    void itShouldDeleteACustomerWhenGivenUsernameAndEmailAddress() {
        // Given
        UUID uuid = UUID.randomUUID();
        String firstName = "Test";
        String lastName = "User";
        String address = "1234 Main Street";
        String city = "Springfield";
        String state = "IL";
        String postalCode = "12345";
        String emailAddress = "test_user@gmail.com";
        String phoneNumber = "+0-000-0000";
        LocalDate dateOfBirth = LocalDate.of(1991, 3, 3);
        String gender = "Male";
        String username = "testUser123";
        String password = "secret";

        Customer customer = new Customer(
                uuid,
                firstName,
                lastName,
                address,
                city,
                state,
                postalCode,
                emailAddress,
                phoneNumber,
                dateOfBirth,
                gender,
                username,
                password
        );
        // When
        underTest.save(customer);
        underTest.deleteCustomerByUsernameAndEmail(username, emailAddress);
        // Then
        Optional<Customer> optionalCustomer = underTest.getCustomerByUsernameAndEmail(username,
                emailAddress);
        assertThat(optionalCustomer)
                .isEmpty();
    }
}