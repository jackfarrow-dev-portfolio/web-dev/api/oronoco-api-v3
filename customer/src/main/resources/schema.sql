CREATE TABLE customer (
    customer_id UUID DEFAULT random_uuid() PRIMARY KEY,
    first_name VARCHAR (50) NOT NULL,
    last_name VARCHAR (50) NOT NULL,
    street_address VARCHAR (50) NOT NULL,
    city VARCHAR (50) NOT NULL,
    state_province VARCHAR (50) NOT NULL,
    postal_code VARCHAR (20) NOT NULL,
    email_address VARCHAR (125) NOT NULL UNIQUE,
    phone_number VARCHAR (20) NOT NULL,
    date_of_birth DATE NOT NULL,
    gender VARCHAR (10) NOT NULL,
    username VARCHAR (125) NOT NULL UNIQUE,
    password VARCHAR (48) NOT NULL
);