package com.oronocoapi.customer;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;
@Repository
public interface CustomerRepository extends CrudRepository<Customer, UUID> {

    @Query( "SELECT c FROM Customer c WHERE c.username = ?1 AND c.emailAddress = ?2")
    Optional<Customer> getCustomerByUsernameAndEmail(String username, String emailAddress);

    @Transactional
    @Modifying
    @Query("DELETE FROM Customer c WHERE c.username = ?1 AND c.emailAddress = ?2")
    int deleteCustomerByUsernameAndEmail(String username, String emailAddress);

    @Transactional
    @Modifying
    @Query("UPDATE Customer c SET c.firstName = ?2, c.lastName = ?3, c.city = ?4, " +
            "c.stateProvince = " +
            "?5, c.emailAddress = ?6, c.phoneNumber = ?7, c.dateOfBirth = ?8, c.gender = ?9 " +
            "WHERE c.id = ?1")
    void updateCustomerDetails(
            UUID customerId,
            String firstName,
            String lastName,
            String city,
            String stateProvince,
            String emailAddress,
            String phoneNumber,
            LocalDate dateOfBirth,
            String gender
    );

    @Transactional
    @Modifying
    @Query("UPDATE Customer c SET c.password = ?3 WHERE c.username = ?1 AND c.emailAddress = ?2")
    void resetCustomerPassword(String username, String emailAddress, String newPassword);
}