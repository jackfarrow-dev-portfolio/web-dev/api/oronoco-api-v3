package com.oronocoapi.customer;

import com.oronoco.amqp.RabbitMqMessageProducer;
import com.oronoco.clients.auth.AuthClient;
import com.oronoco.clients.notification.NotificationRequest;
import com.oronoco.exception.BadRequestException;
import com.oronoco.exception.NotAuthenticatedException;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
public class CustomerService {

    private final CustomerRepository customerRepository;
    private static final Logger logger = LoggerFactory.getLogger(CustomerService.class);
    private final AuthClient authClient;
    private final RabbitMqMessageProducer rabbitMqMessageProducer;


    @Autowired
    public CustomerService(
            CustomerRepository customerRepository,
            AuthClient authClient,
            RabbitMqMessageProducer rabbitMqMessageProducer
            ) {
        this.customerRepository = customerRepository;
        this.authClient = authClient;
        this.rabbitMqMessageProducer = rabbitMqMessageProducer;
    }

    public void registerCustomer(Customer customer) {
        Optional<Customer> optionalCustomer = this.customerRepository.getCustomerByUsernameAndEmail(
                customer.getUsername(),
                customer.getEmailAddress()
        );

        if(optionalCustomer.isPresent()) {
            logger.info("Customer identified by " + customer.getUsername() + " is already present");
            throw new BadRequestException("Unable to register customer; username " + customer.getUsername() +
                    " is already taken");
        }
        this.customerRepository.save(customer);

        NotificationRequest notificationRequest = new NotificationRequest(
                customer.getUsername(),
                "Received request to register new customer " + customer.getUsername(),
               LocalDateTime.now().toString()
        );

        this.rabbitMqMessageProducer.publish(
                notificationRequest,
                "internal.exchange",
                "internal.notification.routing-key"
        );
    }
    public Optional<Customer> getCustomerByUsernameAndEmail(String username, String emailAddress) {
        return this.customerRepository.getCustomerByUsernameAndEmail(username, emailAddress);
    }

    public void updateCustomer(String username, CustomerDetailsRequest request) {
        Optional<Customer> optionalCustomer =
                this.customerRepository.getCustomerByUsernameAndEmail(
                        username,
                        request.getEmailAddress()
                );
        if(optionalCustomer.isEmpty()) {
            logger.info("Customer identified by " + username + " not found");
            throw new BadRequestException("Customer " + username + " " +
                    " was not found");
        }


        UUID id = optionalCustomer.get().getId();

        if(!this.authClient.isAuthenticated(id)) {
            throw new NotAuthenticatedException("Customer " + username + " is not authorized to perform" +
                    " this operation");
        }

            this.customerRepository.updateCustomerDetails(
                    optionalCustomer.get().getId(),
                    request.getFirstName(),
                    request.getLastName(),
                    request.getCity(),
                    request.getStateProvince(),
                    request.getEmailAddress(),
                    request.getPhoneNumber(),
                    request.getDateOfBirth(),
                    request.getGender()
            );
    }
}
