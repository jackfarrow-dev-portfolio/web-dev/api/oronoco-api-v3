package com.oronocoapi.customer;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Optional;

@RestController
@RequestMapping("api/v3/customer")
@Slf4j
public class CustomerController {

    private final CustomerService customerService;
    private final Logger logger = LoggerFactory.getLogger(CustomerController.class);

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("find")
    public Optional<Customer> getCustomerByUsernameAndEmailAddress(@Param("username") String username, @Param("emailAddress") String email) {
        this.logger.info("received request to find customer: " + username + " at " + LocalDateTime.now());
        return this.customerService.getCustomerByUsernameAndEmail(username, email);
    }
    @PostMapping("register")
    public void registerCustomer(@RequestBody Customer customer) {
        this.customerService.registerCustomer(customer);
    }

    @PutMapping("update/{username}")
    public void updateCustomerDetails(
            @PathVariable("username") String username,
            @RequestBody CustomerDetailsRequest request) {
        this.customerService.updateCustomer(username, request);
    }
}
